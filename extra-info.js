'use strict';
var objectPath = require('object-path');

var poundsToGram = 453.5923;

var emptyExtra = {
    recipient: { //получатель
        title: '',
        phone: '',
        location: {
            country: '',
            city:'',
            title: '',
            zip_code: '',
            phone: '',
            email: ''
        }
    },
    sender: { //отправитель
        title: '',
        phone: '',
        location: {
            country: '',
            city:'',
            title: '',
            zip_code: '',
            phone: '',
            email: ''
        }
    },
    weight: {
        volume: 0, //объемный вес, г
        actual: 0 //фактический вес, г
    },
    //specific: {
    //    "russian-post": {
    //        finance: {
    //            payment: 0, //наложенный платеж, руб
    //            value: 0, //объявленная ценность, руб
    //            mass_rate: 0, //?
    //            insr_rate: 0, //?
    //            air_rate: 0, //?
    //            rate: 0 //?
    //        },
    //        item: {
    //            complex_item_name: '',
    //            mail_rank: '',
    //            mail_type: '',
    //            mail_category: '',
    //            post_mark: ''
    //        }
    //    },
    //    ups: {
    //        special: '', // ups Особые инструкции
    //        left_at: '', //ups Сдано в
    //        signed_by: '', //ups, sdek Расписался
    //        service_name: '',
    //        type: ''
    //    },
    //    sdek: {
    //        order_number: '',
    //        signed_by: '' //ups, sdek Расписался
    //    },
    //    pecom: {
    //        services: {
    //            items: [], // услуги
    //            discount: 0,
    //            debt: 0,
    //            free_service: false,
    //            resend_docs: false,
    //            insurance: 0,
    //            hard_pack: false,
    //            sum: 0
    //        },
    //        order_number: '',
    //        seats: 0,
    //        description: ''
    //    },
    //    "dpd-russia": {
    //        options: [],
    //        delivery_type: '',
    //        most_heavy_weight: 0,
    //        chargeable_weight: 0,
    //        seats: 0,
    //        order_number: '',
    //        service_name: '',
    //        origin_location:'',
    //        delivery_date:'',
    //        destination:'',
    //        collection_date:''
    //    },
    //    jde: {
    //        ttn: '', //ТТН
    //        paid: false, //Счет по ТТН
    //        payments: []
    //    },
    //    "parcel-force": {
    //        delivered_on: '',
    //        delivered_at: '',
    //        signed_for_by: '',
    //        service: '',
    //        tracking_status: ''
    //    },
    //    'sf-express':{
    //        origin: '',
    //        originCode: '',
    //        destination: '',
    //        receiveBillFlg: '',
    //        delivered: false,
    //        expectedDeliveryTime: '',
    //        refundable: false,
    //        limitTypeCode: '',
    //        limitTypeName: '',
    //        prioritized: false,
    //        warehouse: false,
    //        signed: false,
    //        lstElementHtml: ''
    //    },
    //    dhl: {
    //        label: '',
    //        type: '',
    //        description: '',
    //        hasDuplicateShipment: false,
    //        duplicate: false,
    //        origin: {
    //            value: '',
    //            label: ''
    //        },
    //        destination: {
    //            value: '',
    //            label: ''
    //        },
    //        signature: {
    //            description: '',
    //            signatory: '',
    //            label: '',
    //            type: '',
    //            link: {
    //                url: '',
    //                label: ''
    //            },
    //            help : ''
    //        }
    //    },
    //    "dhl-global-mail": {
    //        customer_confirmation: '',
    //        delivery_by_dp_paket_plus: '', // new tracking number
    //        service: '',
    //        hdl_gm:''
    //    },
    //    'india-post': {
    //        mailCategory:''
    //    },
    //    'dhl-global-mail-asia': {
    //        qty: '',
    //        dgm_item: '', // tracking
    //        customer_item_ref: '',
    //        service_level: '',
    //        tracking: '', // tracking in destination service
    //        product_type: ''
    //    },
    //    'fedex': {
    //        trackingQualifier:'',
    //        trackingCarrierDesc:'',
    //        shipperCity:'',
    //        shipperStateCD:'',
    //        shipperCntryCD:'',
    //        recipientCity:'',
    //        recipientStateCD:'',
    //        recipientCntryCD:'',
    //        displayEstDeliveryDateTime:'',
    //        displayShipDateTime:'',
    //        displayPickupDateTime:'',
    //        pkgLbsWgt:'', // вес в фунтах
    //        dimensions:'', // размер упаковки дюймы
    //        referenceList:'',// Ссылочный код
    //        shipmentIdList:'',// Идентификационный номер отправления
    //        serviceDesc:'',//  Сервис : "FedEx Home Delivery"
    //        packaging:'',// Упаковка
    //        totalPieces:'',// Общее кол-во мест
    //        originCity:'',
    //        originStateCD:'',
    //        originCntryCD:'',
    //        destCity:'',
    //        destStateCD:'',
    //        destCntryCD:'',
    //        destTZ:'',
    //        originTZ:'',
    //        defaultCDOType:''
    //    },
    //    'posti': {
    //        second_item_code: '',
    //        transport_service: '',
    //        destination_postal_code: '',// tracking in destination service
    //        additional_services: '',
    //        recipient_signature: '',
    //        cod_amount: ''
    //    },
    //    'kazpost': {
    //        type_of_package: '',
    //        special_info: '',
    //        date_of_acceptance: '',// tracking in destination service
    //        office_of_acceptance: '',
    //        delivery_result: '',
    //
    //        category:'',
    //        direction:'',
    //        delivery_method:'',
    //        storage_period: '',
    //
    //        origin_date:'',
    //        origin_x_dep_id: '',
    //        origin_dep_name: '',
    //        origin_postindex: '',
    //        origin_city:''
    //    },
    //    novaposhta: {
    //        delivery_type: '',
    //        delivery_cargo_description_money: '',
    //        sum: '',
    //        ewPaidSumm: '',
    //        RedeliverySum: '',
    //        isEWPaid: false,
    //        isEWPaidCashLess: false,
    //        WareReceiverId: '',
    //        OnlinePayment: false
    //    },
    //    ponyexpress: {
    //        nakladnaya: '',
    //        city_from:'',
    //        city_to:''
    //    },
    //    oneworldexpress: {
    //        number_of_pieces: '',
    //        desc: ''
    //    },
    //    boxberry: {
    //        order_number: '',
    //        phone_of_receive_point:'',
    //        destinationCity:'',
    //        point_of_delivery:''
    //    },
    //    vestovoy: {
    //        number_of_pieces: '',
    //        delivery_date: '',
    //        delivery_time: '',
    //        nakladnaya: '',
    //        order_date: '',
    //        city_from: '',
    //        city_to: ''
    //    },
    //    espeedpost: {
    //        label: '',
    //        process_date:'',
    //        number_of_pieces:'',
    //        service:'',
    //        country_to: '',
    //        country_from: ''
    //    },
    //    "ws-shipping": {
    //        to: '',
    //        from:''
    //    },
    //    "chinapostetracking": {
    //        to: '',
    //        from:''
    //    },
    //    "hermes-dpd": {
    //        content: []
    //    },
    //    "sweden-posten": {
    //        service:''
    //    },
    //    spsr: {
    //        invoicenumber:'',
    //        invoice_id:'',
    //        invoice_owner_id:''
    //    },
    //    cargoexpress: {
    //        number_of_pieces: '',
    //        delivery_date: '',
    //        delivery_time: '',
    //        nakladnaya: '',
    //        order_date: '',
    //        city_from: '',
    //        city_to: ''
    //    },
    //    faspeed: {
    //        from:'',
    //        to:'',
    //        weight:''
    //    },
    //    iml: {
    //        weight: '',
    //        volume: ''
    //    },
    //    dellin: {
    //        aprox_arrival_date:''
    //    }
    //
    //}
};

function ExtraInfo(courierSlug) {
    this.courierSlug = courierSlug;
    var obj = {'specific':{}};
    obj.specific[this.courierSlug] = emptyExtra;
    this.extra = JSON.parse(JSON.stringify(obj));

    this.getExtra = function () {
        for (var prop in this.extra.specific) {
            if (!this.extra.specific.hasOwnProperty(prop)) continue;
            if (prop !== this.courierSlug) {
                delete this.extra.specific[prop];
            }
        }
        return this.extra;
    };

    /**
     * @param {string} property Extra property path
     * @param {object} item object with data
     * @param {string} [path] path in item
     * @param {string} [type]
     */
    this.setExtraValue = function (property, item, path, type) {
        type = typeof type !== 'undefined' ? type : 'string';
        var val;
        if (path) {
            val = objectPath.get(item, path, '');
        } else {
            val = item;
        }

        if ((type === 'string' && val !== '') || (type === 'number' && parseFloat(val) > 0 && (val = parseFloat(val)))) {
            objectPath.set(this.extra, property, val);
        }
    };

    /**
     * @param property
     */
    this.getExtraValue = function (property) {
        return objectPath.get(this.extra, property, '');
    };

    /**
     * @return {string}
     */
    this.FromPoundsToGram = function (value) {
        return (value * poundsToGram).toFixed(2);
    };

    return this;
}

module.exports = ExtraInfo;