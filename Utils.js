var util = require("util");

module.exports = {
    inherits: function (functionA, functionB) {
        util.inherits(functionA, functionB);

        functionA.prototype.parent = function () {
            return functionA.super_.prototype;
        };
    },
    inheritUPUValidators: function (functionA, code) {

        functionA.prototype.formatTrack = function (tracking, callback) {
            tracking = tracking.replace(/\W*/g, '');
            callback(tracking);
        };


        functionA.prototype.isTrackValid = function (tracking, callback) {

            this.formatTrack(tracking, function(formattedTracking){
                tracking = formattedTracking;
            });

            if (tracking.match(new RegExp('^[a-zA-Z]{2}\\d{9}' + code + '$'))) {
                callback({tracking:tracking});
                return;
            }
            callback(false);
        };

        functionA.prototype.isMyTracking = function (tracking, callback) {

            this.formatTrack(tracking, function(formattedTracking){
                tracking = formattedTracking;
            });

            if (tracking.match(new RegExp('^[a-zA-Z]{2}\\d{9}' + code + '$'))) {
                callback({tracking:tracking});
                return;
            }
            callback(false);
        };


    },
    trim: function(string) {
        if (!string) {
            return '';
        }
        return string.replace(/\s{2,}/g, ' ').replace(/^[\s\.]+|[\s\.]+$/g, '');
    }
};
