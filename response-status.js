module.exports = { };
module.exports.code = {
    SUCCESS: 0,
    SERVICE_NOT_FOUND: 1,
    EMPTY: 10,
    INVALID_TRACKING: 11,
    HTTP_ERROR: 12,
    AUTH_ERROR: 13,
    PARSE_ERROR: 14,
    TRACKING_NOT_FOUND: 15,
    SERVICE_ERROR: 16
};
