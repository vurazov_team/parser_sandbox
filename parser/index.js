'use strict';
var util = require("util");
var Browser = require('post2go-curl-browser');
var ResponseStatusCode = require('../response-status').code;
var ParserInterface = require("../IDeliveryService");
var ParserResult = require('../parser-result');
var moment = require('moment');
var Utils = require("../Utils");


var Extra = require('../extra-info');

function CourierParser(pResult) {
    this.parserResult = pResult;
}
util.inherits(CourierParser, ParserInterface);

CourierParser.prototype.getInfo4Tracking = function (tracking, callback) {

    var self = this;
    var browser = new Browser();
    var parserResult = this.parserResult ? new this.parserResult(callback) : new ParserResult(callback);

    var URL_PATTERN_INFO = "http://track.kazpost.kz/api/v2/%s";
    var URL_PATTERN_EVENTS = "http://track.kazpost.kz/api/v2/%s/events";

    var extraInfo = new Extra('courierSlug');
    var destination = null;
    var current_weight = null;

    this.formatTrack(tracking, function(formattedTracking){
        tracking = formattedTracking;
        loadTrackingPage();
    });

    function loadTrackingPage() {
        // если нужно заменить чтото в URL как в примере ниже - то используем плейсхолдер %s - 'http://data.courier.ru/search=%s'
        // и далее обрабатываем с помощью следующей конструкции
        // var url = util.format(trackingUrl, tracking);

        // для GET запросов используем следующую конструкцию
        // browser.get(url, function (error, result) {
        //     parserResult.checkBrowserError(error, result, parseTrackingPage);
        // });

        var url = util.format(URL_PATTERN_INFO, tracking);
        browser.get(url,
            function (error, result) {
                parserResult.checkBrowserError(error, result, parseInfo);
            });


        // для POST запросов используем следующую конструкцию
        // browser.postEx({
        //     url: trackingUrl,
        //     contentType: 'application/javascript',
        //     postData: JSON.stringify({ appKey: appKey, docid: tracking })
        // }, function (error, result) {
        //     parserResult.checkBrowserError(error, result, parseTrackingPage);
        // });
        //
        // это чисто для примера - данные можно подсовывать те которые необходимо
        // browser.postEx({
        //         url:url,
        //         postData: 'trackno='+tracking
        //     },
        //     function (error, result) {
        //         parserResult.checkBrowserError(error, result, parseTrackingPage);
        //     });

    }

    function parseInfo(result) {
        
        parserResult.setBrowserResult(result);
        
        var response = '';
        try {
            response = JSON.parse(result.body.toString().replace(/\\n/g, '').replace(/&quot;/g,'"'));
        } catch (err) {
            parserResult.error(ResponseStatusCode.SERVICE_ERROR, err);
            return;
        }

        // делаем проверки на нужные статусы и ответы от сервера до разбора

        if (response.hasOwnProperty('error') ) {
            parserResult.error(ResponseStatusCode.SERVICE_ERROR, response.error);
            return;
        }
        
        // собиает всю дополнительную инфу касающуюся исключительно данного курьера
        parseExtraInfo(response);

        var url = util.format(URL_PATTERN_EVENTS, tracking);
        browser.get(url,
            function (error, result) {
                parserResult.checkBrowserError(error, result, parseDataPage);
            });
    }

    function parseDataPage(result) {

        parserResult.setBrowserResult(result);

        // так можно получить строку с ответом от сервера, если не делать toString - она будет в бинарном виде
        // result = result.body.toString();
        // var $ = require('cheerio').load(result);

        // если работаем с HTML - то используем cheerio

        // var $ = require('cheerio').load(result.body.toString(), {
        //     xmlMode: true, normalizeWhitespace: true,
        //     decodeEntities: false
        // });
        //
        // if (result.body.toString().indexOf('Номер заказа не найден') !== -1 ) {
        //     parserResult.error(ResponseStatusCode.TRACKING_NOT_FOUND, "Номер заказа не найден");
        //     return;
        // }
        //
        // var response = $('table.cell-align-center td');


        // если с JSON - то так
        try {
            var response = JSON.parse(result.body.toString().replace(/\\n/g, '').replace(/&quot;/g,'"'));
        } catch (err) {
            parserResult.error(ResponseStatusCode.SERVICE_ERROR, err);
            return;
        }

        if (response.hasOwnProperty('errors')) {
            parserResult.error(ResponseStatusCode.TRACKING_NOT_FOUND, response.errors.docid.trim());
            return;
        }

        if (response.events.length === 0) {
            parserResult.error(ResponseStatusCode.EMPTY, "нет статусов");
            return;
        }
        var err = null;

        
        // определяем место назначения
        // destination = getCountryCodeBySymbols($('table.cell-align-center td tr'));

        var statuses = [];

        response.events.every(function (el, idx) {

            var title = '';
            var location = '';
            var time = null;
            var date = null;

            if (!el.date)
                return true;

            date = el.date;

            el.activity.every ( function(el)  {

                if (!el.time)
                    return true;

                time = el.time;
                var zip = el.zip;
                var city = el.city;
                location = el.name;
                var x_dep_id = el.x_dep_id;
                if (el.status.length === 1) {
                    title = el.status[0];
                } else {
                    title = el.status.reduce (function (previousValue, currentValue, index, array){
                        return previousValue + ', ' + currentValue;
                    });
                }
                if (!title || title.length == 0){
                    err = {'message':'title not found EltaCourierParser ('+ el.status+')'};
                    return false;
                }

                var timeParsed = self.prepareTime(time, date);
                if (timeParsed === null) {
                    err = {'message':'Error on parse date in KazPostParser ('+ time+')'};
                    return false;
                } else {
                    statuses.unshift({
                        time: timeParsed,
                        title: title,
                        weight: current_weight,
                        location : {
                            title: (city !== '') ? city + ', ' + location : location,
                            zip_code: zip,
                            city: city,
                            x_dep_id: x_dep_id
                        }
                    });
                }

                return true;

            });


            return true;
        });

        if (err) {
            parserResult.error(ResponseStatusCode.PARSE_ERROR, err);
            return;
        }

        parserResult.success(tracking, statuses, destination, extraInfo.getExtra());
    }

    function parseExtraInfo(info) {

        if  (info.sender){

            if  (info.sender.name && info.sender.name.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.sender.title', info.sender.name);
            }
            if  (info.sender.country && info.sender.country.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.sender.location.country', info.sender.country);
            }
            if  (info.sender.address && info.sender.address.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.sender.location.title', info.sender.address);
            }
            if  (info.sender.x_postindex && info.sender.x_postindex.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.sender.location.zip_code', info.sender.x_postindex);
            }

        }
        if  (info.receiver){

            if  (info.receiver.name && info.receiver.name.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.recipient.title', info.receiver.name);
            }
            if  (info.receiver.country && info.receiver.country.length !== 0){
                destination = getCountryCodeBySymbols(info.receiver.country);
                extraInfo.setExtraValue('specific.kazpost.recipient.location.country', info.receiver.country);
            }
            if  (info.receiver.address && info.receiver.address.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.recipient.location.title', info.receiver.address);
            }
            if  (info.receiver.x_postindex && info.receiver.x_postindex.length !== 0){
                extraInfo.setExtraValue('specific.kazpost.recipient.location.zip_code', info.receiver.x_postindex);
            }

        }

        if  (info.direction && info.direction.length !== 0){
            extraInfo.setExtraValue('specific.kazpost.direction', info.direction);
            if (destination == null && (info.direction === 'LOCAL' || info.direction === 'IMPORT' ) ) {
                destination = 'KAZ';
            }
        }
        if  (info.storage_period && info.storage_period.length !== 0){
            extraInfo.setExtraValue('specific.kazpost.storage_period', info.storage_period);
        }
        if  (info.package_type && info.package_type.length !== 0){
            extraInfo.setExtraValue('specific.kazpost.type_of_package', info.package_type);
        }
        if  (info.category && info.category.length !== 0){
            extraInfo.setExtraValue('specific.kazpost.category', info.category);
        }
        if  (info.delivery_method && info.delivery_method.length !== 0){
            extraInfo.setExtraValue('specific.kazpost.delivery_method', info.delivery_method);
        }
        if  (info.weight && info.weight.length !== 0){
            current_weight = info.weight+' кг';
            extraInfo.setExtraValue('specific.kazpost.weight.actual', current_weight);
        }

        if  (info.origin){
            if  (info.origin.date && info.origin.date.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.origin_date', info.origin.date);
            }
            if  (info.origin.x_dep_id && info.origin.x_dep_id.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.origin_x_dep_id', info.origin.x_dep_id);
            }
            if  (info.origin.city && info.origin.city.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.origin_city', info.origin.city);
            }
            if  (info.origin.dep_name && info.origin.dep_name.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.origin_dep_name', info.origin.dep_name);
            }
            if  (info.origin.postindex && info.origin.postindex.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.origin_postindex', info.origin.postindex);
            }
        }

        if  (info.delivery){
            if  (info.delivery.date && info.delivery.date.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_date', info.delivery.date);
            }
            if  (info.delivery.time && info.delivery.time.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_time', info.delivery.time);
            }
            if  (info.delivery.phone && info.delivery.phone.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_phone', info.delivery.phone);
            }
            if  (info.delivery.x_dep_id && info.delivery.x_dep_id.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_x_dep_id', info.delivery.x_dep_id);
            }
            if  (info.delivery.city && info.delivery.city.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_city', info.delivery.city);
            }
            if  (info.delivery.dep_name && info.delivery.dep_name.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_dep_name', info.delivery.dep_name);
            }
            if  (info.delivery.postindex && info.delivery.postindex.length !== 0) {
                extraInfo.setExtraValue('specific.kazpost.delivery_postindex', info.delivery.postindex);
            }
        }
    }

    function getCountryCodeBySymbols(symbols){
        if (symbols.indexOf('Российская Федерация')!== -1)
           return 'RUS';
        else if (symbols.indexOf('BELARUS')!== -1)
           return 'BLR';

        // sentry.captureMessage('[EltaCourier] unknown destination: ' + symbols,
        //     {extra: {
        //         tracking: tracking,
        //         symbols: symbols
        //     }});
        
        return null;
    }

};


/**
 *   RR054255247KZ
 */
CourierParser.prototype.isTrackValid = function (tracking, callback) {

    this.formatTrack(tracking, function(formattedTracking){
        tracking = formattedTracking;
    });

    var patterns = [
        /^[a-zA-Z]{2}\d{9}KZ$/i
    ];
    for(var i = 0; i < patterns.length; i++) {
        if (tracking.match(patterns[i])) {
            callback({tracking:tracking});
            return;
        }
    }

    callback(false);
};


CourierParser.prototype.formatTrack = function (tracking, callback) {
    tracking = tracking.replace(/\W*/g, ''); // remove all special chars
    callback(tracking);
};


CourierParser.prototype.isMyTracking = function (tracking, callback) {

    this.formatTrack(tracking, function(formattedTracking){
        tracking = formattedTracking;
    });

    var patterns = [
        /^[a-zA-Z]{2}\d{9}KZ$/i
    ];
    for(var i = 0; i < patterns.length; i++) {
        if (tracking.match(patterns[i])) {
            callback({tracking:tracking});
            return;
        }
    }
    callback(false);
};

/**   "16-10-2014" "18:51"  **/
CourierParser.prototype.prepareTime = function(time, date) {
    var momentObj = moment(date + ' ' +time, 'DD.MM.YYYY HH:mm');
    if (!momentObj.isValid()){
        console.error('Error on parse date in KazPostParser ('+ time +'). error '+momentObj.invalidAt());
        return null;
    }
    return momentObj.format('YYYY-MM-DD HH:mm:ss');
};

module.exports = CourierParser;