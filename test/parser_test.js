var parserTemplate = require('../parser');

var testHelper = require('./test_helper');
var util = require('util');
var ParserResult = require('../parser-result');
parser = new parserTemplate(ParserResult);
var sinon = require('sinon');
var expect = require('chai').expect;
var should = require('should');
var Parcel = require('../parcel');

var track = {
    valid: 'RR054255247KZ',
    invalid: 'RA698563628CN'
};

describe('Parser', function () {
    
    describe.skip('#isTrackValid()', function(){
        it('should return true ' + track.valid, function(){
            parser.isTrackValid(track.valid, function(response) {
                response.should.not.be.false;
            });
        });

        it('should return false ' + track.invalid, function(){
            parser.isTrackValid(track.invalid, function(response) {
                response.should.be.false;
            });
        });
    });

    describe.skip('#isMyTracking()', function(){
        it('should return true ' + track.valid, function(){
            parser.isMyTracking(track.valid, function(response) {
                response.should.not.be.false;
            });
        });

        it('should return false ' + track.invalid, function(){
            parser.isMyTracking(track.invalid, function(response) {
                response.should.be.false;
            });
        });
    });
    
    describe('#getInfo4Tracking()', function(){

        var timeout = 3000;
        it.skip('should run not longer then ' + timeout, function(done){
            this.timeout(timeout); // время на выполнение
            parser.getInfo4Tracking(track.valid,  function(status, parcel, destination) {
                // console.log(util.inspect(status, false, 5, true));
                // console.log(util.inspect(parcel, false, 5, true));
                testHelper.successTest(status);
                done();
            });
        });

        it.skip('should call parser-result.success or parser-result.error once', function(done){

            var stub = sinon.stub(ParserResult.prototype, "success", function (arg) {
                this.callback();
            });
            var stub2 = sinon.stub(ParserResult.prototype, "error", function (arg) {
                this.callback();
            });
            var stub3 = sinon.spy(ParserResult.prototype, "setBrowserResult");
            var stub4 = sinon.spy(ParserResult.prototype, "checkBrowserError");

            var timeout = 3500;
            this.timeout(timeout);
            setTimeout(function () {

                // проверям что был вызван success 1 раз
                var s1 = stub.callCount;
                var s2 = stub2.callCount;

                if (s1 != 0){
                    expect(s1).to.be.eq(1, 'success MUST be called ONCE');
                }
                if (s2 != 0){
                    expect(s2).to.be.eq(1, 'error MUST be called ONCE');
                }

                expect(s1+s2).to.be.eq(1, 'MUST be called ONE method (not both)');

                expect(stub3.called).to.be.eq(true, 'setBrowserResult must be called at least once');
                expect(stub4.called).to.be.eq(true, 'checkBrowserError must be called at least once');

                stub.reset();
                stub2.reset();
                stub3.reset();
                stub4.reset();

                done();

            }, timeout-500);


            var parser = new parserTemplate(ParserResult);
            parser.getInfo4Tracking(track.valid,  function(status, parcel, destination) {
                // console.log(util.inspect(status, false, 5, true));
                // console.log(util.inspect(parcel, false, 5, true));
                // testHelper.successTest(status);
                // done();
            });

        });


        it('should return object with correct format for VALID track' + timeout, function(done){
            parser.getInfo4Tracking(track.valid,  function(status, parcel, destination) {
                // console.log(util.inspect(status, false, 5, true));
                testHelper.successStatusTest(status);
                arguments.length.should.be.exactly(3);
                parcel.should.be.an.instanceOf(Parcel);
                parcel.should.have.property('tracking').and.not.be.empty();
                parcel.should.have.property('extra');
                parcel.should.have.property('statuses').and.an.instanceOf(Array);
                done();
            });
        });

        it('should return object with correct format for INVALID track' + timeout, function(done){
            parser.getInfo4Tracking(track.invalid,  function(status) {
                // console.log(util.inspect(status, false, 5, true));
                testHelper.nonSuccessStatusTest(status);
                arguments.length.should.be.exactly(1);
                done();
            });
        });



    });

});