var util = require('util');
var ResponseStatusCode = require('../response-status').code;
var should = require('should');
function TestHelper(){}

TestHelper.prototype.successStatusTest = function(status) {
    status.should.have.property('code');
    status.code.should.be.exactly(ResponseStatusCode.SUCCESS);
    status.should.have.property('message');
    status.should.have.property('proxy');
};

TestHelper.prototype.nonSuccessStatusTest = function(status) {
    status.should.have.property('code');
    status.code.should.not.be.exactly(ResponseStatusCode.SUCCESS);
    status.should.have.property('message');
    status.should.have.property('proxy');
    status.should.have.property('body');
};


TestHelper.prototype.invalidTrackingTest = function(status) {
    testProperties(status);
    status.code.should.be.exactly(ResponseStatusCode.INVALID_TRACKING);
};

TestHelper.prototype.trackingNotFoundTest = function(status) {
    testProperties(status);
    status.code.should.be.exactly(ResponseStatusCode.TRACKING_NOT_FOUND);
};

TestHelper.prototype.emptyResponseTest = function(status) {
    testProperties(status);
    status.code.should.be.exactly(ResponseStatusCode.EMPTY);
};

module.exports = new TestHelper();



