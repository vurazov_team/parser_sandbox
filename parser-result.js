'use strict';
var ResponseStatusCode = require('./response-status').code;

var ParserResult = function (callback) {
    this.browserResult = null;
    this.isFinished = false;
    this.callback = callback;
};

ParserResult.prototype.setBrowserResult = function (_result) {
    this.browserResult = _result;
};

ParserResult.prototype.checkBrowserError = function (error, result, okCallback) {
    if (error) {
        var message = 'http error';
        if (error.proxy !== null) {
            message += ' with proxy';
        }
        this.callback({
            code: ResponseStatusCode.HTTP_ERROR,
            message: message,
            proxy: error.proxy,
            error: error
        });
        return;
    }

    if (result.status != 200) {
        this.callback({
            code: ResponseStatusCode.HTTP_ERROR,
            message: "Code " + result.status,
            body: result.body.toString(),
            proxy: result.proxy
        });
        return;
    }

    okCallback(result);
};

ParserResult.prototype.error = function (code, message) {
    if (this.isFinished) {
        throw new Error('already finished');
    }

    this.isFinished = true;
    this.callback({
        code: code,
        message: message ? message : '',
        body: this.browserResult && typeof this.browserResult.body === 'string' ? this.browserResult.body.toString() : null,
        proxy: this.browserResult && this.browserResult.proxy ? this.browserResult.proxy : null
    });

};

/**
 * @param formattedTracking
 * @param statuses
 * @param destination
 * @param extra
 */
ParserResult.prototype.success = function (formattedTracking, statuses, destination, extra) {

    if (this.isFinished) {
        throw new Error('already finished');
    }

    this.isFinished = true;

    var status;
    if (statuses.length > 0) {
        status = { code: ResponseStatusCode.SUCCESS, message: "parsed successfully" };
    } else {
        status = { code: ResponseStatusCode.EMPTY, message: "empty response" };
    }
    var Parcel = require('./parcel');
    var parcel = new Parcel();
    parcel.tracking = formattedTracking;
    parcel.extra = extra;
    parcel.statuses = statuses;

    this.callback({
        code: status.code,
        message: status.message,
        proxy: this.browserResult.proxy
    }, parcel, destination);
};

module.exports = ParserResult;