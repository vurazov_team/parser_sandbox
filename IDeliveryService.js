
function IDeliveryService(){}

/**
 * function check all possible variations of tracking number format
 * even if them equal to variations of other couriers
 *
 * @param tracking
 * @param callback
 * @return boolean true|false
 */
IDeliveryService.prototype.isTrackValid = function(tracking, callback){
    throw new Error('method isTrackValid isn\'t implemented');
};

/**
 * function only check variations of tracking number format which is
 * definitely belongs to current courier
 *
 * @param tracking
 * @param callback
 * @return Object {tracking:formattedTracking}|null
 */
IDeliveryService.prototype.isMyTracking = function (tracking, callback){
    throw new Error('method isMyTracking isn\'t implemented');
};

/**
 * @param tracking
 * @param callback
 */
IDeliveryService.prototype.getInfo4Tracking = function (tracking, callback){
    throw new Error('method getInfo4Tracking isn\'t implemented');
};

/**
 * @param tracking
 * @param callback
 */
IDeliveryService.prototype.formatTrack = function (tracking, callback){
    throw new Error('method isn\'t implemented');
};

module.exports = IDeliveryService;