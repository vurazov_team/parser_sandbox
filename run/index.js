'use strict';

var parserTemplate = require('../parser');
var parser = new parserTemplate();
var util = require('util');

var track = {
    valid: 'RR054255247KZ',
    invalid: 'RA698563628CN'
};


parser.getInfo4Tracking(track.valid,  function(status, parcel, destination) {
    console.log(util.inspect(status, false, 5, true));
    console.log(destination);
    console.log(util.inspect(parcel, false, 5, true));
});
